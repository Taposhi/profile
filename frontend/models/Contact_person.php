<?php

namespace frontend\models;

use yii;
use common\models\User;

class Contact_person extends \yii\db\ActiveRecord {

    public static function tableName() {

        return 'contact_person';
    }

    public function rules() {

        return [
            [['name', 'email', 'phone', 'company_id'], 'required'],
            [['name', 'email'], 'string'],
            [['phone'], 'string', 'max' => 30],
            [['image'], 'file', 'extensions' => 'jpg, gif, png'],
        ];
    }

    public function attributeLabels() {
        return [
            'Name' => 'Name',
            'phone' => 'Phone Number',
            'email' => 'Email Address',
            'image' => 'Upload Image'
        ];
    }
    
    public function getcompany() {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

}
