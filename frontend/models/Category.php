<?php

namespace frontend\models;

use yii;

class Category extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'category';
    }

    public function rules() {
        return [

            [['name'], 'required'],
        ];
    }

}
