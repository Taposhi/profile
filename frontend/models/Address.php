<?php

namespace frontend\models;

use yii;
use common\models\User;

class Address extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'address';
    }

    public function rules() {
        return [

            [['address', 'phone'], 'required'],
            [['user_id'], 'integer'],
            [['address'], 'string'],
            [['phone'], 'string', 'max' => 30],
        ];
    }

    public function attributeLabels() {
        return [
            'address' => 'Address',
            'phone' => 'Phone Number'
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

}

?>