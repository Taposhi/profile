<?php

namespace frontend\models;

use yii;
use common\models\User;

class Product extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'product';
    }

    public function rules() {
        return [

            [['name', 'category', 'specification'], 'required'],
            [['name'], 'string', 'max' => 30],
            [['model_no', 'image'], 'string'],
            [['company_id'], 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'name' => 'Product Name',
            'category' => 'Category',
            'image' => 'Upload Product Image',
            'specification' => 'Specification',
            'model_no' => 'Model No.',
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

}
