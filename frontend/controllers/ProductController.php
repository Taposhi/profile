<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Address;
use frontend\models\Category;
use frontend\models\Contact_person;
use frontend\models\Product;
use yii\web\UploadedFile;

/**
 * Product controller
 */
class ProductController extends Controller {

    public function actionIndex() {

        $id = \Yii::$app->user->identity->id;
        $category = Category::find()->all();
        $model = new Product;
        $product = Product::find()->where(['company_id' => $id])->all(); // all() get error[call to a member function load() on array]
        // echo "<pre>";
        // var_dump($model);
        // echo "</pre>";
        //  exit();

        if ($model->load(\yii::$app->request->post()) && $model->validate()) {

            $image = UploadedFile::getInstance($model, 'image');

            $image->saveAs('uploads/product/' . $image->baseName . '.' . $image->extension);
            $model->image = $image->baseName . '.' . $image->extension;
            $model->save();
            if ($model->save()) {
                return $this->redirect(['product/index']);
            }
        }
        return $this->render('product', ['model' => $model, 'category' => $category, 'product' => $product]);
    }

    public function actionDetails($id) {
        $category = Category::find()->all();
        $model = Product::find()->where(['id' => $id])->one();

        if ($model->load(\yii::$app->request->post()) && $model->validate()) {
            $image = UploadedFile::getInstance($model, 'image');

            $image->saveAs('uploads/product/' . $image->baseName . '.' . $image->extension);
            $model->image = $image->baseName . '.' . $image->extension;
            $model->save();
        }

        return $this->render('product_details', ['model' => $model, 'category' => $category]);
    }

}
