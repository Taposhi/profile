<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Address;
use frontend\models\Contact_person;
use yii\web\UploadedFile;

/**
 * Profile controller
 */
class ProfileController extends Controller {

    public function actionIndex() {
        $address_model = new Address();
        $id = \Yii::$app->user->identity->id;
        $profile_model = User::find()->where(['id' => $id])->one();

        //$adrs = $profile_model->addresses;   //get addresss realtion data
        // foreach($profile_model->addresses as $adr){
        // 	print_r($adr);
        // }
        //   echo "<pre>";
        // 	var_dump($profile_model);
        //   echo "</pre>";
        // 	exit();
        //$address= Address::find()->where(['user_id'=>$id])->all();

        if ($address_model->load(\yii::$app->request->post())) {
            $valid_info = $address_model->validate();
            if ($valid_info) {
                $address_model->save();
                return $this->redirect(\Yii::$app->request->referrer);
            }
        }

        return $this->render('profile', ['profile' => $profile_model, 'model' => $address_model]);
    }

    public function actionContact_person() {

        $model = new Contact_person;
        $id = \Yii::$app->user->identity->id;
        $person_model = Contact_person::find()->where(['company_id' => $id])->one();
 
        if($model->load(\yii::$app->request->post())){
      
             $image= UploadedFile::getInstance($model, 'image');
          
              $image->saveAs( 'uploads/contact/' . $image->baseName . '.' . $image->extension);
              $model->image= $image->baseName . '.' . $image->extension;
            
              $model->save(); 
            
        }

        return $this->render('contact_person', ['person' => $person_model, 'model' => $model]);
    }

    public function actionContact_update($id) {

        $model = Contact_person::find()->where(['id' => $id])->one();

        if ($model->load(\yii::$app->request->post()) && $model->validate()) {

               $image= UploadedFile::getInstance($model, 'image');
               // var_dump($image);
               // exit();
               if(!empty($image)){
                 $image->saveAs( 'uploads/contact/' . $image->baseName . '.' . $image->extension);
                 $model->image= $image->baseName . '.' . $image->extension;
               }
               echo "<pre>";
              var_dump($model);
              echo "</pre>";
             exit();
               $model->save();
               if( $model->save()){
                  return $this->redirect(['profile/contact_person']);
               }
        }

        return $this->render('contact_update', ['person' => $model]);
    }

    public function actionInfo_update($id) {

        $model = User::find()->where(['id' => $id])->one();

         if ($model->load(\yii::$app->request->post())) {

            if ($model->validate()) {
               $logo= UploadedFile::getInstance($model, 'logo');
               // var_dump($logo);
               // exit();
               $logo->saveAs( 'uploads/logo/' . $logo->baseName . '.' . $logo->extension);
               $model->logo= $logo->baseName . '.' . $logo->extension;
               $model->save();
               if( $model->save()){
                  return $this->redirect(['profile/index']);
               }
            }
        }

        return $this->render('profile_update', ['model' => $model]);
    }

}
