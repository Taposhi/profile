<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use yii\helpers\Url;



$this->title= 'Profile';
// $profile = $model->profile;
//  echo "ok";
//  echo "<pre>";
// 	   	var_dump($model);
//  	   	echo "</pre>";
// 	  	exit();
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>


	<div class="container">
		<div class="row" style="margin-bottom: 80px;">
			<div class="col-md-12">
				<h3>Profile Information</h3>
				<table class="table">
					<tr><td>Name</td><td>Username</td><td>Email Address</td><td>Logo</td><td>Action</td></tr>
					<tr>
						<td><?=$profile->name; ?></td>
						<td><?=$profile->username; ?></td>
						<td><?=$profile->email;?></td>
						<td><img width="100px" height="100px" src="<?= Yii::$app->request->baseUrl.'/uploads/logo/'.$profile->logo;?>"></td>
						<td><a class="btn btn-sm btn-primary" href="<?= Url::toRoute(['profile/info_update','id'=>$profile->id]);?>">Update</a></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<?php 
					if(!empty($profile->addresses)){
				?>
				<table class="table">
					<tr><td>SN.</td><td>Address</td><td>Phone Number</td></tr>
					<?php
 					$id=1;		
					foreach ($profile->addresses as $value) { ?>
						<tr>
							<td><?=$id;?></td>
							<td><?=$value['address']; ?></td>
							<td><?=$value['phone']; ?></td>
						</tr>
					<?php 
					$id++;
						}
					?>
				</table>
				<?php 
			}
			?>
			</div>
			<div class="col-md-4">
				<h3>Add New Address</h3>
					<?php $form= ActiveForm::begin(['id'=>'address-form']); ?>
					<?= $form->field($model, 'address')->textarea(['row' => 2,'autofocus' => true]) ;?>
		        	<?= $form->field($model,'phone')->textInput(); ?>
		        	<?= $form->field($model, 'user_id')->hiddenInput(['value'=> $profile->id])->label(false);?>
		            
		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>

		            <?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>