<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use frontend\models\Contact_person;
use yii\helpers\Url;
use common\models\User;


$this->title= 'Update Company Information ';
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    	<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php $form= ActiveForm::begin(['id'=>'profile-update-form']); ?>
					<?= $form->field($model, 'name')->textInput() ;?>
					<?= $form->field($model, 'username')->textInput() ;?>
					<?= $form->field($model, 'email')->input('email');?>		        	
		        	<?= $form->field($model, 'logo')->fileInput();?>
		            
		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>

		            <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>	
</div>
