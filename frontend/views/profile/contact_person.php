<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use frontend\models\Contact_person;
use yii\helpers\Url;



$this->title= 'Contact Person Information';
// $profile = $model->profile;
//  echo "ok";
//  echo "<pre>";
// 	   	var_dump($model);
//  	   	echo "</pre>";
// 	  	exit();
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    	<div class="container">
			<div class="row">
				<div class="col-md-8">
				<?php
					if(!empty($person)){
				?>
					<table class="table">
	
						<tr><td><img width="200px" src="<?= Yii::$app->request->baseUrl.'/uploads/contact/'.$person->image;?>"></td><td></td></tr>
						<tr><td>Name:</td><td><?=$person->name; ?></td></tr>
						<tr><td>Email Address:</td><td><?=$person->email; ?></td></tr>
						<tr><td>Phone Number:</td><td><?=$person->phone; ?></td></tr>
						<tr><td><a class="btn btn-sm btn-primary" href="<?= Url::toRoute(['profile/contact_update','id'=>$person->id]);?>">Edit</a></td><td></td></tr>

					</table>

				<?php
					}
				?>
				</div>
				<div class="col-md-4">
					<?php $form= ActiveForm::begin(['id'=>'contact-person-form','options' => ['enctype' => 'multipart/form-data']]); ?>
					<?= $form->field($model, 'name')->textInput() ;?>
					<?= $form->field($model, 'email')->input('email');?>
		        	<?= $form->field($model,'phone')->textInput(); ?>
		        	<?= $form->field($model, 'image')->fileInput() ?>
		        	<?= $form->field($model, 'company_id')->hiddenInput(['value'=> \Yii::$app->user->identity->id])->label(false);?>		      
		            
		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>

		            <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
</div>