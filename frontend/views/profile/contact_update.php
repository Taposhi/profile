<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use frontend\models\Contact_person;
use yii\helpers\Url;
use yii\web\UploadedFile;



$this->title= 'Update Contact Person Information ';
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    	<div class="container">
			<div class="row">
				<div class="col-md-8">
					<?php $form= ActiveForm::begin(['id'=>'contact-update-form','options' => ['enctype' => 'multipart/form-data']]); ?>
					<?= $form->field($person, 'name')->textarea(['row' => 2,'autofocus' => true]) ;?>
					<?= $form->field($person, 'email')->input('email');?>
		        	<?= $form->field($person,'phone')->textInput(); ?>
		        	<?= $form->field($person, 'image')->fileInput();?>
		        	<?= $form->field($person, 'company_id')->hiddenInput(['value'=> $person->company_id])->label(false);?>	      
		            
		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>
		            <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>	
</div>
