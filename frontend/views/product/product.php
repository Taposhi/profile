<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use yii\helpers\Url;



$this->title= 'Product List';
// $profile = $model->profile;
//  echo "ok";
//  echo "<pre>";
// 	   	var_dump($model);
//  	   	echo "</pre>";
// 	  	exit();
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
    	<div class="row">
    		<div class="col-md-8">
    			<table class="table">
    				<tr>
    				<td>Sl.</td>
    				<td>Name</td>
    				<td>Specification</td>
    				<td>Model No.</td>
                    <td>Action</td>
    				</tr>
                    <?php 
                        if(!empty($product)){
                            $i=1;
                            foreach ($product as $value) {
                    ?>
                    <tr>
                    <td><?= $i;?></td>
                    <td><?= $value->name;?></td>
                    <td><?= $value->specification;?></td>
                    <td><?= $value->model_no;?></td>
                    <td><a class="btn btn-sm btn-primary" href="<?= Url::toRoute(['product/details','id'=>$value->id]);?>">Deails</a></td>
                    </tr>
                   <?php       $i++;  }
                        }
                    ?>
    				
    			</table>
    		</div>
            <?php
            $cat_option=[];
                if(!empty($category)){
                    foreach ($category as $value) {
                        $cat_option[$value->id] = $value->name;
                    }
                }
            ?>
    		<div class="col-md-4">
    			<h3>Add New Product</h3>
					<?php $form= ActiveForm::begin(['id'=>'product-form']); ?>
					<?= $form->field($model,'name')->textInput(); ?>
					<?= $form->field($model,'category')->dropDownList($cat_option,['prompt'=>'Select Option']); ?>
					<?= $form->field($model, 'image')->fileInput() ?>
					<?= $form->field($model,'model_no')->textInput(); ?>
                    <?= $form->field($model, 'specification')->textarea(['row' => 5]) ;?>
		        	<?= $form->field($model, 'company_id')->hiddenInput(['value'=>\Yii::$app->user->identity->id])->label(false);?>

		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>

		            <?php ActiveForm::end(); ?>
			</div>
    		</div>
    	</div>
    </div>
</div>