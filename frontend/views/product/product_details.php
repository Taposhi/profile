<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use frontend\models\Address;
use yii\helpers\Url;



$this->title= 'Product View';
// $profile = $model->profile;
//  echo "ok";
//  echo "<pre>";
// 	   	var_dump($model);
//  	   	echo "</pre>";
// 	  	exit();
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="container">
    	<div class="row">
    		<div class="col-md-8">
    			<table class="table">
    				<tr><td><img width="200px" src="<?= Yii::$app->request->baseUrl.'/uploads/product/'.$model->image;?>"></td><td></td></tr>
                    <tr><td>Name:</td><td><?=$model->name;?></td></tr>
                    <tr><td>Category:</td><td></td></tr>
                    <tr><td>Model No.</td><td><?=$model->model_no;?></td></tr>
                    <tr><td>Specification:</td><td><?=$model->specification;?></td></tr>
    			</table>
    		</div>
            <?php
            $cat_option=[];
                if(!empty($category)){
                    foreach ($category as $value) {
                        $cat_option[$value->id] = $value->name;
                    }
                }
            ?>
    		<div class="col-md-4">
    			<h3>Edit Product</h3>
					<?php $form= ActiveForm::begin(['id'=>'product-form']); ?>
					<?= $form->field($model,'name')->textInput(); ?>
					<?= $form->field($model,'category')->dropDownList($cat_option,['prompt'=>'Select Option']); ?>
					<?= $form->field($model, 'image')->fileInput() ?>
					<?= $form->field($model,'model_no')->textInput(); ?>
                    <?= $form->field($model, 'specification')->textarea(['row' => 5]) ;?>
		        	<?= $form->field($model, 'company_id')->hiddenInput(['value'=>\Yii::$app->user->identity->id])->label(false);?>

		        	<div class="form-group">
		                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
		            </div>

		            <?php ActiveForm::end(); ?>
			</div>
    	</div>
    </div>
</div>
