<?php

/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'Search';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container">
    	<?php $form = ActiveForm::begin([
				    'method' => 'post',
				    'action'=>['site/search']
					]); ?>

		<?= $form->field($model, 'search')->textInput()->label(false);?>
		<?php ActiveForm::end(); ?>

    </div>
</div>